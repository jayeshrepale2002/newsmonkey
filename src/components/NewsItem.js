import React, { Component } from 'react'

export class NewsItem extends Component {

    render() {
        let {title, description, imageUrl, newsUrl, author, date, source} = this.props;              // Setting state using props
        return (
            <div className='my-3'>
                <div className="card">
                    <span className="position-absolute top-0 translate-middle badge rounded-pill bg-danger" style={{left: '90%', zIndex: '1'}}>{source}</span>
                    <img src={!imageUrl? "https://img.republicworld.com/republic-prod/stories/promolarge/xhdpi/upyzo1savmkostio_1689160024.jpeg" : imageUrl} className="card-img-top" alt="img"/>
                        <div className="card-body">
                            <h5 className="card-title">{title}</h5>
                            <p className="card-text">{description}</p>
                            <p className='card-text'><small className="text-muted">By {author? author: "Unknown"} on {new Date(date).toGMTString()}</small></p>
                            <a href={newsUrl} rel="noreferrer" className='btn btn-sm btn-dark' target='_blank'>Read More</a>
                        </div>
                </div>
            </div>
        )
    }
}

export default NewsItem
